require('./navigation.less');

<navigation>
	
	<!-- логика -->
	<script>
		this.items = []
		if (this.opts.items) {
			this.items = this.opts.items
		}
	</script>

	<ul>
		<li each={ item, i in items }>
			<a href="{ item.url }">{ item.name }</a>
		</li>
	</ul>


	<style scoped type="less">
	</style>

</navigation>