var _config = {};

_config.menuitems = [
	{ url:'/', name:'Главная'},
	
	{ url:'/page/1', name:'Страница 1'},

	{ url:'/page/2', name:'Страница 2'}
]


_config.page_content = {
	page1: { title:'Page 1', body:' page 1 body'},
	page2: { title:'Page 2', body:' page 2 body'}
}


module.exports = _config;