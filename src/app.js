'use strict'

import _config 	from './config.js'

import './app.less'
import './app.tag'

// Импорт всех тегов
var tags_context = require.context('./components/', true, /\.tag$/);
	tags_context.keys().forEach(function(path){
		tags_context(path)
	});


// Старт
document.addEventListener('DOMContentLoaded', ()=>{
	riot.mount('app', {_config:_config})

	var content_block = document.getElementById('content');

	/*
	 * Роутинг
	 */
		riot.route.base('/')
		riot.route.start(true)


		// // Главная
		riot.route('/', function() {
			// require('./components/index_page.tag');

			riot.mount( content_block, 'index_page')
		})

		// Странички
		riot.route('/page/*',function(pagenum) {
			// require('./components/simple_page.tag');

			riot.mount(
				content_block,
				'simple_page',
				{ content: _config.page_content['page'+pagenum] }
			)
		})

}) // document.addEventListener('DOMContentLoaded', ()=>{