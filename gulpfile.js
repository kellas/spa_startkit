
var gulp        = require('gulp'),
    webpack     = require('webpack'),
    notify      = require('gulp-notify'),
    rm          = require('rimraf'),
    minifyHTML  = require('gulp-minify-html'),
    imagemin    = require('gulp-imagemin'),
    sitemap     = require('gulp-sitemap'),
    browserSync = require('browser-sync').create();


// Адрес сайта
var siteurl = 'https://alexstep.com';

// Адреса для которых необходимо сгененрировать статические страницы
var prerender_routes = [
  '/',
  '/page/1',
  '/page/2'
];


var src       = './src/',
    dest      = './dist/',
    homepage  = 'index.html';

var config = {
  // Смотри gulp task generate_static_site
  generate_static_site:false,
  prerender_routes: prerender_routes,

  src: src,
  dest: dest,
  
  webServer: {
    server: dest,
    index: homepage,
    port: 3000,
    logLevel: 'debug',
    logPrefix: 'JHW',
    open: true,
    files: [dest + '/*.js', './index.html']
  },
  
  less: { src: src + '**/*.less' },
  scss: { src: src + '**/*.scss' },

  script: {
    entry: {
      'app': src + 'app.js'
    },
    output: {
      path: dest,
      publicPath:'/',
      filename: 'bundle.js'
    },

    sourceMap: true,
    watch: src + '**/*.js'
  },

  html: {
    watchHome: homepage,
    watchAll: src + '**/*.html'
  }
}



/*
 * Сервер для разработки
 */
  gulp.task('default', ['watch']);



  gulp.task('watch', ['web-server'], function() {
    // var reload = browserSync.reload({ stream:true });

    gulp.watch(config.script.watch, ['webpack']);
    gulp.watch(config.less.src, ['webpack']);
    gulp.watch(config.scss.src, ['webpack']);
    gulp.watch(config.src + '/**/*.tag', ['webpack']);
    gulp.watch(config.html.watchHome,   ['html']);
    gulp.watch(config.html.watchAll, ['html']);
  });

  gulp.task('web-server', ['build'],function() {
    browserSync.init(config.webServer);
  });

  gulp.task('webpack', function() {
    webpack(require('./webpack.config')(config), function(err, stats) {
      if (err) { handleErrors(); } else {
        browserSync.reload({ stream:true });
      }
    });
  });



/*
 * Билд для продакшена
 */
  gulp.task('build', ['webpack', 'img', 'public2dest', 'html']);



/*
 * Генерация статического сайта
 * phantomjs пробгается по  config.prerender_routes
 * и сохраняет их как html страницки в __dirname+'/dist/',
 */

  gulp.task('generate_static_site', ['build'], function() {
    config.generate_static_site = true;
    webpack(require('./webpack.config')(config), function(err, stats) {
      if (err) { 
        handleErrors(); 
      } else {
        console.log('\033c');
        console.log(' >>> Generate static site  in progress...');
        setTimeout(function(){
          gulp.start('sitemap');
          setTimeout(function(){
            console.log(' ✔ Static site genrated!');
          });

        }, config.prerender_routes.length*3000 );
      }

    })
  });



// Генерация sitemap
// https://www.npmjs.com/package/gulp-sitemap
gulp.task('sitemap', function () {
  // Ищем все html файлы в дирректории с билдом
  gulp.src(dest+'**/*.html', {
    read: false
  })
  // и пишем пути к ним в sitemap
  .pipe(sitemap({
    siteUrl: siteurl,
  }))
  .pipe(gulp.dest(dest)).on('end', function(){
    console.log(' ✔ sitemap.xml generated ');
  });
});

// Перенос html
gulp.task('html', function() {
  return gulp.src([src + '**/*.html'])
    .pipe(minifyHTML({quotes: true}))
    .pipe(gulp.dest(dest))
    .pipe(browserSync.reload({ stream:true }))
    .on('end', function(){
      console.log(' ✔ Html moved');
    });
});

// Перенос прочей статики в корень
gulp.task('public2dest', function() {
  return gulp.src([src + 'public/**/*.*'])
    .pipe(gulp.dest(dest))
    .on('end', function(){
      console.log(' ✔ Files from src/public moved to dist/ ');
    });
});

// Очистка дирректории с билдом
gulp.task('clean', function(next) {
  rm(dest, function() { next(); });
});


// Перенос и оптимизация картинок
gulp.task('img:dev', ['clean'], function() {
  return gulp.src([src + '/images/**'])
    .pipe(watch())
    .pipe(reload());
});

gulp.task('img', ['clean'], function() {
  return gulp.src([src + '/images/**'])
    .pipe(imagemin())
    .pipe(gulp.dest(dest + '/images')).on('end', function(){
      console.log(' ✔ Images optimized ');
    });
});




function handleErrors() {
  var args = Array.prototype.slice.call(arguments);
  notify.onError({
    title: 'compile error',
    message: '<%= error.message %>'
  }).apply(this, args);
  this.emit('end');
}