
var webpack           = require('webpack')
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var autoprefixer      = require('autoprefixer');

module.exports = function(config) {
  var wpconf = {
    entry:  config.script.entry,
    output: config.script.output,

    module: {
      preLoaders: [
        // Riot tags
        {
          test: /\.tag$/,
          exclude: /node_modules/,
          loader: 'riotjs-loader',
          query: { type: 'none' }
        }
      ],

      loaders: [
        // JS
        {
          test: /\.js$/,
          loader: 'babel-loader',
          exclude: /node_modules/,
          query:{
            presets: ['es2015']
          }
        },

        // Styles
        {
          test: /\.css$/,
          loader: ExtractTextPlugin.extract('style', 'css!postcss')
        },
        {
          test: /\.less$/,
          loader: ExtractTextPlugin.extract('css!postcss!less')
        },
        {
          test: /\.scss$/,
          loader: ExtractTextPlugin.extract('css!postcss!sass')
        },
        {
          test: /\.(png|jpg|gif|svg)$/,
          loader: 'url',
          query: {
            // limit for base64 inlining in bytes
            limit: 10000,
            // custom naming format if file is larger than
            // the threshold
            name: 'images/[name].[ext]?[hash]'
          }
        },
        {
          test: /\.(ttf|eot|woff|woff2)$/,
          loader: 'url',
          query: {
            // limit for base64 inlining in bytes
            limit: 10000,
            // custom naming format if file is larger than
            // the threshold
            name: 'fonts/[name].[ext]?[hash]'
          }
        }
      ]
    },

    postcss: function () {
      return [autoprefixer];
    },


    plugins: [
      new webpack.ProvidePlugin({
        riot: 'riot',
        $: 'jquery'
      }),
      new ExtractTextPlugin('styles.css', {allChunks:true}),
    ]
 
  };

  /*
   * Режим продакшн сборки
   * с минификацией и оптимизацией
   */
  if (process.env.NODE_ENV === 'production') {
    wpconf.plugins.push(new webpack.optimize.OccurenceOrderPlugin());
    
    wpconf.plugins.push(new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }));
      
    wpconf.plugins.push(new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings:       false,
        drop_console:   true,
        drop_debugger:  true,
        unsafe:         true
      }
    }));
  } else {
    wpconf.devtool = '#source-map'
  }
  

  /*
   * Генерация статического сайта
   * phantomjs пробгается по  config.prerender_routes
   * и сохраняет их как html страницки в __dirname+'/dist/',
   */
  if (config.generate_static_site) {
    var prerenderSpaPlugin = require('prerender-spa-plugin')

    wpconf.plugins.push(new prerenderSpaPlugin(
      
      __dirname+'/dist/',

      config.prerender_routes,

      {
        captureAfterTime: 2000,

        maxAttempts: 2,

        // http://phantomjs.org/api/command-line.html#command-line-options 
        phantomOptions: '--disk-cache=true',
        // captureAfterElementExists: '#content',

        // http://phantomjs.org/api/webpage/property/settings.html 
        phantomPageSettings: {
          loadImages: true
        }
      }
    ));
  }

  return wpconf
}
