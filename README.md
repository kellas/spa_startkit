Starterkit for build modern SPA.
Includes <a href="http://riotjs.com/">RiotJS</a>, webpack, gulp, browser-sync, less, <a href="https://www.npmjs.com/package/prerender-spa-plugin">static site generator</a>, sitemap.xml generator.
<br><br>

1. First install all dependencies.
<pre>
npm install
</pre>
<b>Warining Traffic</b>: node_modules over 250Mb,
devDependencies has "prerender-spa-plugin" - he needs phantomjs  
<br>

2. Start dev server
<pre>
npm start
</pre>
<br>

3. Build productrion ready SPA
<pre>
npm run build
</pre>

4. Generate static pages(for seo)
<pre>
npm run generate_static_site
</pre>
prerender-spa-plugin run phantomjs, "parse" your SPA, and save html pages in dist.
See <b>var prerender_routes</b> in gulpfile.js

5. Deploy
<pre>
sudo npm i -g firebase
npm run deploy_setup # or firebase init
npm run deploy # or firebase deploy
</pre>